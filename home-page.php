<?php
/**
 * Template Name: Login Page (Home Page)
 */

get_header();

if ( is_user_logged_in() ) :

	get_template_part( 'templates/home', 'main_content' );

else :

	get_template_part( 'templates/the_message' );

endif;

get_footer();