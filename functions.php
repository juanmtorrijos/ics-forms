<?php 

define ( 'THEMEROOT', get_template_directory_uri() );
define ( 'IMAGESPATH', THEMEROOT . '/images' );

/**
 * Register Styles and Scripts
 */
require get_template_directory() . '/includes/register_my_styles_and_scripts.php'; 

/**
 * Custom Fields
 */


/**
 * Other Includes
 */
require get_template_directory() . '/includes/meta-viewport.php';
require get_template_directory() . '/includes/google-fonts.php';
require get_template_directory() . '/includes/wp-title-hack-for-home.php';
require get_template_directory() . '/includes/ajax-login.php';
require get_template_directory() . '/includes/remove_admin_bar.php';
require get_template_directory() . '/includes/add_html5_support.php';
require get_template_directory() . '/includes/acf_add_options_page.php';
require get_template_directory() . '/includes/add_post_thumbnail_support.php';
require get_template_directory() . '/includes/register-nav-menus.php';
require get_template_directory() . '/includes/ics-forms-info-logout-nav-link.php';