var ajax_obj;!function($){$(function(){var a=$("form.cs-login-form"),o=$(".login-form").find(".login-loading-animation"),s=$("input#icsforms_wp-submit"),r=$(".status-message"),t="animated shake",n={border:"1px solid #C10000",background:"#E34250",color:"white"};a.on("submit",function(a){a.preventDefault(),o.fadeIn(450),s.css("opacity",.35),$.ajax({type:"POST",dataType:"json",url:ajax_obj.ajaxurl,data:{action:"ajaxlogin",security:ajax_obj.security,username:$("input#icsforms_user_login").val().toString(),password:$("input#icsforms_user_pass").val().toString()},success:function(a){a.loggedin===!1?(r.text(a.message).css(n).show().addClass(t),o.fadeOut(450),s.css("opacity",1).val("Try Again")):document.location.href=ajax_obj.redirecturl},error:function(){r.text("Sorry an error ocurred. Please try again later.").css(n).show(),o.fadeOut(450)}})})})}(jQuery);