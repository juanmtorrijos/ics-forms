<?php if ( is_single() ) : ?>

	<p class="back-button">
		<a href="<?php echo home_url( '/' ); ?>">&laquo; Back to ICS Forms.</a>
	</p>

<?php endif; ?>

<?php if( have_posts() ) : while( have_posts() ) : the_post();  ?>

	<?php if (have_rows('certificates_and_documents_table')) : ?>

	<table>
		<thead>
			<tr>
				<th class="short-number">No.</th>
				<th>Document Name</th>
				<th class="title">Click Link to Download</th>
			</tr>
		</thead>
		<tbody>

		<?php while( have_rows('certificates_and_documents_table') ) : the_row(); ?>

			<tr>
				<td class="table-number centered">
					<?php echo get_row_index(); ?>
				</td>
				<td>
					<?php the_sub_field('documentation_name'); ?>
				</td>
				<td class="centered">
					<?php if (get_sub_field('link_to_dropbox_initial')) : ?>
						<a href="<?php the_sub_field('link_to_dropbox_initial'); ?>">
							<i class="fa fa-cloud-download" aria-hidden="true"></i> &nbsp; from Dropbox
						</a>
					<?php else : ?>
						<span class="missing-link-error">
							<i class="fa fa-exclamation-circle " aria-hidden="true"></i> &nbsp; No Link Available
						</span>
					<?php endif; ?>
				</td>
				</td>
			</tr>

		<?php endwhile; ?>

		</tbody>
	</table>

	<?php endif; ?>

<?php endwhile; endif; ?>