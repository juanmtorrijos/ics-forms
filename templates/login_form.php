<div class="login-form">
	<div class="status-message"></div>
	<div class="login-loading-animation"></div>
	<form class="cs-login-form" action="login">

		<label>Username</label>
		<input type="text" name="icsforms_user_login" id="icsforms_user_login" class="input" />

		<label>Password</label>
		<input type="password" name="icsforms_user_pass" id="icsforms_user_pass" class="input" />

		<div class="icsforms-submit-button rememberme">
			<label><input name="icsforms_rememberme" id="icsforms_rememberme" type="checkbox" checked="checked" class="icsforms-rememberme" value="forever" />Remember Me</label>

			<input type="submit" name="wp-submit" id="icsforms_wp-submit" value="Log In" />
			<input type="hidden" name="login-with-ajax" value="login" />
		</div>

	</form>
</div>