<?php if (get_field('important_notice_1', 'option')) : ?>

	<div class="important-notes-1">
		<p><?php the_field('important_notice_1', 'option'); ?></p>
	</div>

<?php endif; if(get_field('important_notice_2', 'option')) : ?>

	<div class="important-notes-2">
		<p><?php the_field('important_notice_2', 'option'); ?></p>
	</div>

<?php endif; ?>