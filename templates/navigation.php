<?php 
	$signout_fa = '<i class="fa fa-sign-out"></i>';
	$user_fa = '<i class="fa fa-user"></i>';
	
	$current_user = wp_get_current_user();
?>

<div class="logo-logged-in">
	
	<?php get_search_form(); ?>

</div>

<div class="navigation-logout">
	<div class="navigation">
		
		<?php get_template_part( 'templates/nav-menu' ); ?>

	</div>
</div>