<div class="main">

	<div class="main-content">

		<?php if( have_posts() ) : while( have_posts() ) : the_post();

		the_title( '<h3>', '</h3>' );

		endwhile; else :

			get_template_part('templates/404_message');

		endif; wp_reset_query();

		get_template_part( 'templates/important_notes' );

		get_template_part( 'templates/ics-forms-table' );

		?>

	</div>

</div>