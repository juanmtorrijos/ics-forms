<?php 

$ics_forms_args = array(
	'post_type' => 'ics-forms-info',
	'posts_per_page' => -1,
	'order' => 'ASC',
	'orderby' => 'date',
);

$ics_forms_children = new WP_Query( $ics_forms_args );

if ( $ics_forms_children->have_posts() ) : 

?>

<ul class="ics-forms">

	<?php while ( $ics_forms_children->have_posts() ) : $ics_forms_children->the_post(); ?>

		<li>
			<a href="<?php echo the_permalink(); ?>">
				<div class="forms-title">
					<?php the_title( '<h3>', '</h3>' ); ?>
				</div>
				<div 
					class="figure"
					<?php if ( has_post_thumbnail() ) : ?>
					style="background:
							url('<?php echo the_post_thumbnail_url(); ?>')
							center
							center
							no-repeat;
							background-size: cover;
						"
					<?php endif; ?>
				></div>
				<div class="forms-text">
					<?php the_excerpt(); ?>
				</div>
				<div class="forms-link">
					<p>
						<a class="button" href="<?php echo the_permalink(); ?>">See Forms &raquo;</a>
					</p>
				</div>
			</a>
		</li>

	<?php endwhile; ?>

</ul>

<?php endif; wp_reset_query(); ?>