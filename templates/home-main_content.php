<div class="main">

	<div class="main-content">

		<?php if( have_posts() ) : while( have_posts() ) : the_post();

		the_title( '<h2>', '</h2>' );

		the_content();
		
		endwhile; else :

			get_template_part('templates/404_message');

		endif; wp_reset_query();

		get_template_part( 'templates/ics-forms' );

		?>
	</div>

</div>