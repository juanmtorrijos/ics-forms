<div class="main">

	<div class="main-content">

		<?php if( have_posts() ) : while( have_posts() ) : the_post();

			if ( !is_page_template( 'home-page.php' ) ) :

				the_title('<h1>', '</h1>');

			endif;

		the_content();

		endwhile; else :

			get_template_part('templates/404_message');

		endif; wp_reset_query();

		?>

	</div>

</div>