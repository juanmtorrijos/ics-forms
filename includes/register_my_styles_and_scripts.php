<?php

function register_my_styles_and_scripts() {

	wp_enqueue_style( 'main_style', THEMEROOT . '/css/main.css', array(), '2015', 'all' );

	wp_enqueue_style( 'google_fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:700', array(), '2015', 'all');

	if ( !is_user_logged_in() && ( is_home() || is_front_page() ) ) {

		wp_enqueue_script( 'ajax_login', THEMEROOT . '/js/min/ajax-login-min.js', array('jquery'), '20160505', true );

		wp_localize_script( 'ajax_login', 'ajax_obj', array(
			'ajaxurl'		=> admin_url( 'admin-ajax.php' ),
			'redirecturl' 	=> home_url(),
			'security'		=> wp_create_nonce( 'icsforms_login' ),
			'errormessage' 	=> 'Sorry, username and password ane incorrect. Please try again, or contact administrator for password reset.',
		) );
		
	}

}

add_action( 'wp_enqueue_scripts', 'register_my_styles_and_scripts' );