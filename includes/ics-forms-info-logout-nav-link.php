<?php

function ics_forms_info_logout( $items, $args ) {
	if ($args->theme_location == 'primary') {
		if (is_user_logged_in()) {
			$items .= '<li class="menu-item menu-item-type-post_type menu-item-object-page logout-button"><a href="'. wp_logout_url('/') .'"><i class="_mi _before dashicons dashicons-migrate"></i> <span>Log Out</span></a></li>';
		}
	}
	return $items;
}

add_filter( 'wp_nav_menu_items', 'ics_forms_info_logout', 10, 2 );