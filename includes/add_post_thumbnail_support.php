<?php

add_theme_support( 'post-thumbnails' );

set_post_thumbnail_size( 400, 250, array( 'center', 'center' ) );