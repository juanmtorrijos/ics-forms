<?php

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Important Notices',
		'menu_title'	=> 'Notices',
		'menu_slug' 	=> 'ics-forms-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
}